<?php
// src/Controller/Home.php
namespace App\Controller;

use App\Entity\PriceListItem;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Form\PricelistItemFormType;


class Home extends AbstractController
{

    /**
     * @Route("/home")
     */
    public function index(Request $request): Response
    {

        //la liste des prix du produit pour tous les concurrents
        $pricelist = $this->getDoctrine()->getRepository(PriceListItem::class)->findAll();

        $pricelistItem = new PriceListItem();
        $formpricelist = $this->createForm(PricelistItemFormType::class, $pricelistItem);
        $formpricelist->handleRequest($request);


        $defaultData = ['message' => 'Type your message here'];
        $form = $this->createFormBuilder($defaultData)
            ->add('base_price', NumberType::class)
            ->add('stat', ChoiceType::class, [
                'choices' => [
                    'Etat moyen' => 1,
                    'Bon ètat' => 2,
                    'Très bon ètat' => 3,
                    'Comme neuf' => 4,
                    'Neuf' => 5,
                ],
            ])->add('Calculer', SubmitType::class)
            ->getForm();
        $form->handleRequest($request);


        if ($formpricelist->isSubmitted() && $formpricelist->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($pricelistItem);
            $entityManager->flush();

            $pricelist = $this->getDoctrine()->getRepository(PriceListItem::class)->findAll();

            $this->data = [
                'pricelist' => $pricelist,
                "form_pricelist_title" => "Ajouter un élément à la liste de prix",
                "form_pricelist" => $formpricelist->createView(),
                "form_title" => "Choisir l'état de votre produit",
                "form" => $form->createView(),
            ];

            return $this->render('home/home.html.twig', $this->data);


        }

        if ($form->isSubmitted() && $form->isValid()) {

            // stratégie 1 centime
            $data = $form->getData();

            $filtredarray = array_filter($pricelist, function ($var) use ($data) {
                return ($var->getStat() == $data["stat"] && $var->getPrice() > $data["base_price"]);
            });


            if (count($filtredarray) > 0) {
                $minA = min(array_map(function ($item) {
                        return $item->getPrice();
                    }, $filtredarray)) - 0.01;
            } else {

                $minA = $data["base_price"];

            }

            // stratégie 1 euro
            $filtredarray = array_filter($pricelist, function ($var) use ($data) {
                return ($var->getStat() > $data["stat"] && $var->getPrice() > $data["base_price"]);
            });

            if (count($filtredarray) > 0) {
                $minB = min(array_map(function ($item) {
                        return $item->getPrice();
                    }, $filtredarray)) - 1;
            } else {

                $minB = $data["base_price"];

            }


            usort($pricelist, function ($a, $b) {

                if ($a->getStat() == $b->getStat())
                    return $a->getPrice() < $b->getPrice() ? 1 : -1;
                return $a->getStat() < $b->getStat() ? 1 : -1;

            });


            $this->data = [
                'pricelist' => $pricelist,
                "form_pricelist_title" => "Ajouter un élément à la liste de prix",
                "form_pricelist" => $formpricelist->createView(),
                "form_title" => "Choisir l'état de votre produit",
                "form" => $form->createView(),
                "strategyA" => $minA,
                "strategyB" => $minB,
            ];

            return $this->render('home/home.html.twig', $this->data);


        } else {

            $this->data = [
                'pricelist' => $pricelist,
                "form_pricelist_title" => "Ajouter un élément à la liste de prix",
                "form_pricelist" => $formpricelist->createView(),
                "form_title" => "Choisir l'état de votre produit",
                "form" => $form->createView(),
            ];

            return $this->render('home/home.html.twig', $this->data);

        }


    }


}




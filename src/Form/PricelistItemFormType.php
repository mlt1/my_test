<?php

namespace App\Form;

use App\Entity\PriceListItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PricelistItemFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('vendor')
            ->add('price')
            ->add('Ajouter', SubmitType::class)
            ->add('stat', ChoiceType::class, [
                'choices' => [
                    'Etat moyen' => 1,
                    'Bon ètat' => 2,
                    'Très bon ètat' => 3,
                    'Comme neuf' => 4,
                    'Neuf' => 5,
                ],
            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PriceListItem::class,
        ]);
    }
}
